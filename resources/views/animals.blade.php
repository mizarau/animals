<div style="margin-left:2rem;">
  <h2>Male</h2>

  <table>
    @foreach ($maleOwned as $name)
    <tr><td>{{ $name }}</td></tr>
    @endforeach
  </table>

  <h2>Female</h2>
  <table>
    @foreach ($femaleOwned as $name)
    <tr><td>{{ $name }}</td></tr>
    @endforeach
  </table>
</div>
