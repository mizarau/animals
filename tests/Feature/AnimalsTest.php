<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AnimalsTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCreateAnimal()
    {
        $animal1 = [
          'name' => 'xxx',
          'gender' => 'Male',
          'age' => '25',
          'pets' => [
            'name' => 'Oreo',
            'type' => 'Cat'
          ]
        ];

        $animal2 = [
          'name' => '', //empty name
          'gender' => 'Male',
          'age' => '25',
          'pets' => [
            'name' => 'Oreo',
            'type' => 'Cat'
          ]
        ];

        $response = $this->postJson('/api/animals/save', $animal1);

        //assert that the record is created successfully
        $response
           ->assertStatus(201)
           ->assertJson([
               'message' => ['created']
           ]);


         $response = $this->postJson('/api/animals/save', $animal2);

         //assert that an error is produced when the name is empty
         $response
            ->assertStatus(422)
            ->assertJson([
                'message' => ['Validation failed']
            ])

            ->assertJson([
                'errors' => [
                    'name'    => ['This value should not be blank.'],
                ]
            ]);
    }
}
