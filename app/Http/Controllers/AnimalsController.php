<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class AnimalsController extends Controller
{
    /**
     * list the cats grouped by the gender of their owners
     * @return view list of cats grouped by owner gender
     */
    public function list()
    {

      $client = new Client();

      $maleOwnedcats = [];
      $femaleOwnedcats = [];

      //attempt to fetch the data from the provided source, show relevant error message on failure
      try {
        $response = $client->get('https://curio-hosting.s3-ap-southeast-2.amazonaws.com/animals.json', ['http_errors' => true]);
      }

      catch(RequestException $e) {
        if ($e->hasResponse()) {
          $exception = (string) $e->getResponse()->getBody();
          return response()->json($exception, $e->getCode());
        } else {
          return response()->json($e->getMessage(), 503);
        }
      }

      $animalArray = json_decode($response->getBody(), true);
      if ($animalArray === null) {
          return response()->json('data is empty',204);
      }

      /** loop through the array to gather the names of all cats and write it into two
       ** separate arrays based on the gender of the owner **/
      foreach ($animalArray as $key => $item) {

          if(!empty($item['pets'])) {
            foreach($item['pets'] as $pet) {
              if('Cat' === $pet['type']) {
                if('Male' === $item['gender']){
                  $maleOwnedcats[] = $pet['name'];
                }

                if('Female' === $item['gender']){
                  $femaleOwnedcats[] = $pet['name'];
                }

              }
            }
          }
      }

      //utilise the laravel collection to sort the array
      $maleOwnedcatsSorted = (collect($maleOwnedcats)->sort()->toArray());
      $femaleOwnedcatsSorted = (collect($femaleOwnedcats)->sort()->toArray());

      //send the data to a view to display the required result
      return view('animals')
        ->withFemaleOwned($femaleOwnedcatsSorted)
        ->withMaleOwned($maleOwnedcatsSorted);
    }

    /**
     * Save an animal
     * @param  Request $request form data
     * @return json           new record
     */
    public function save(Request $request)
    {
      $client = new Client(['baseURL' => config('APP_URL') ]);

      $animal = [
        'name' => $request->name,
        'gender' => $request->gender,
        'age' => $request->age,
        'pets' => $request->pets
      ];

      try {
        $response = $client->post('/api/animals', ['form_params' => $animal]);
      }

      catch(RequestException $e) {
        if ($e->hasResponse()) {
          if(404 == $e->getCode) {
            return "Not found";
          }

          if(422 == $e->getCode) {
            return $e->getResponse()->getBody();
          }
        } else {
          return response()->json($e->getMessage(), 503);
        }
      }

      return response()->json('created',201);

    }
}
